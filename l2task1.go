package l2task1

import (
	"log"
	"time"

	"github.com/beevik/ntp"
)

const addr string = "0.beevik-ntp.pool.ntp.org"

func L2task1() {
	time2, err := ntp.Time(addr)
	if err != nil {
		log.Println(err)
	}
	log.Println(time2)
	response, err := ntp.Query(addr)
	if err != nil {
		log.Println(err)
	}
	time1 := time.Now().Add(response.ClockOffset)
	log.Println(time1)
}
